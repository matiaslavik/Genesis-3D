Genesis-3D Engine
===========

# Introduction

This is a fork of Changyou's Genesis-3D engine, which was released as alpha in 2013 and has since been abandoned.

This forks adds the following:
- 64 bit support
- C++17 support
- Linux support
- Upgraded Mono

Some things are still WIP:
- PhysX has been disabled
- Audio has been disabled
- On Linux you can't run built projects. Only Editor projects.

## Cloning
The repository contains submodules, so you need to clone recursively or pull the submodules as well.
- git clone https://github.com/mlavik1/Genesis-3D
- git submodule update --init --recursive

## Prerequisites (Linux)
- TinyXML: sudo apt install libtinyxml-dev
- FreeType: sudo apt install freetype2-demos
- OpenAL: sudo apt install libopenal-dev
- Bison: sudo apt install bison
- Flex: sudo apt install flex
- FreeImage: sudo apt install libfreeimage-dev
- GLFW: sudo apt-get install libglfw3 libglfw3-dev
- Mono: sudo apt install mono-complete

## How to Compile
1. Download the third party packages from https://nl.tab.digital/s/CFzCrqYs3GSQtAk, and unpackage it.
2. Combine the Genesis-3D project dir with the unpackaged zup dir (Copy the "bin"  and "Engine" folder over to the repository's root folder).
3. Cmake the Engine dir
4. Go to the build dir, open vs sln, and compile the engine

**Example:**
- cd Engine
- mkdir build
- cd build
- cmake ../Engine
- cmake --build . --target Genesis

## Running the engine
1. Download this test project (or create your own from the Genesis-3D editor, if you have it): https://nl.tab.digital/s/NNxRr3Qd4bwq8p2
2. Download the default assets from: https://nl.tab.digital/s/2wGYGWSeMqfjagC (or use the ones from the Genesis-3D editor install, if you have it)
3. Execute: ``./Genesis --gamedir "YOUR_PATH/GenesisTest" --scenename "asset:Test.scene" --sysdir "YOUR_PATH/DefaultAssets/System" --shddir "YOUR_PATH/DefaultAssets/Shader"`` (replace "YOUR_PATH" with the directory where you extraced the project and default assets)

For more info, see the old official documentation: https://github.com/yangrongwei/genesis-3d/tree/master/doc

## Notes
- The shader compiler could definitely need an update:
    - Genesis-3D uses hlsl2glsl to convert CG/HLSL shaders to GLSL. I've had to modify it a bit, and this is the custom fork: https://codeberg.org/matiaslavik/hlsl2glslfork
    - hlsl2glslfork is very old and is no longer maintained (Unity stopped using it).
    - On Windows, NVidia's CG library is used. This library is also deprecated and no longer maintained.
- A very old version of Mono is used for C# scripting.
