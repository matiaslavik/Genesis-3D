#pragma once

// LAVIK: TODO: There should be no need for std::binary_function anymore (removed since C++17)
// Replace uses of this with lambda or something else.
namespace std_fake
{
    template<class Arg1, class Arg2, class Result> 
    struct binary_function
    {
        using first_argument_type = Arg1;
        using second_argument_type = Arg2;
        using result_type = Result;
    };
}
