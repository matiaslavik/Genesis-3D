/****************************************************************************
Copyright (c) 2011-2013,WebJet Business Division,CYOU
 
http://www.genesis-3d.com.cn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
#include "stdneb.h"
#include "linuxmemorypool.h"

namespace Linux
{
LinuxMemoryPool::LinuxMemoryPool()
{

}

LinuxMemoryPool::~LinuxMemoryPool()
{

}


//------------------------------------------------------------------------------
/**
*/
uint LinuxMemoryPool::ComputeAlignedBlockSize(uint blockSize)
{
	return 0;
}

//------------------------------------------------------------------------------
/**
*/
uint LinuxMemoryPool::GetNumBlocks() const
{
	return 0;
}

//------------------------------------------------------------------------------
/**
*/
uint LinuxMemoryPool::GetBlockSize() const
{
	return 0;
}

//------------------------------------------------------------------------------
/**
*/
uint LinuxMemoryPool::GetAlignedBlockSize() const
{
	return 0;
}

//------------------------------------------------------------------------------
/**
*/
inline uint LinuxMemoryPool::GetPoolSize() const
{
	return 0;
}

//------------------------------------------------------------------------------
/**
*/
bool LinuxMemoryPool::IsPoolBlock(void* ptr) const
{
	return false;
}

//------------------------------------------------------------------------------
/**
    NOTE: name must be a static string!
*/
void LinuxMemoryPool::Setup(Memory::HeapType heapType_, uint blockSize_, uint numBlocks_)
{
    
}

//------------------------------------------------------------------------------
/**
*/
void* LinuxMemoryPool::Alloc()
{
   

    return NULL;
}

//------------------------------------------------------------------------------
/**
*/
void LinuxMemoryPool::Free(void* ptr)
{
    
}
#if NEBULA3_MEMORY_STATS
uint LinuxMemoryPool::GetAllocCount() const
{
	return 0;
}
#endif
}