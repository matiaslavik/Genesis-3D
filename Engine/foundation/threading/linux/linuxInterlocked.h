/****************************************************************************
Copyright (c) 2011-2013,WebJet Business Division,CYOU
 
http://www.genesis-3d.com.cn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
#pragma once

#ifndef __GNUC__
#error "Interlocked only supports GCC at the moment." // LAVIK: TODO
#endif

#include "core/types.h"

//------------------------------------------------------------------------------
namespace Linux
{
class LinuxInterlocked
{
public:
    /// interlocked increment, return result
    static int Increment(int volatile& var);
    /// interlocked decrement, return result
    static int Decrement(int volatile& var);
    /// interlocked add
    static int Add(int volatile& var, int add);
    /// interlocked exchange
    static int Exchange(int volatile* dest, int value);
    /// interlocked compare-exchange
    static int CompareExchange(int volatile* dest, int exchange, int comparand);
};

//------------------------------------------------------------------------------
/**
*/
__forceinline int
LinuxInterlocked::Increment(int volatile& var)
{
   __atomic_fetch_add(&var, 1, __ATOMIC_RELAXED);
   return var;
}

//------------------------------------------------------------------------------
/**
*/
__forceinline int
LinuxInterlocked::Decrement(int volatile& var)
{
	__atomic_fetch_sub(&var, 1, __ATOMIC_RELAXED);
	return var;
}

//------------------------------------------------------------------------------
/**
*/
__forceinline int
LinuxInterlocked::Add(int volatile& var, int add)
{
    __sync_fetch_and_add(&var, add, __ATOMIC_RELAXED);
	return var;
}

//------------------------------------------------------------------------------
/**
*/
__forceinline int
LinuxInterlocked::Exchange(int volatile* dest, int value)
{
    return __atomic_exchange_n(dest, value, __ATOMIC_RELAXED);
}
//------------------------------------------------------------------------------
/**
*/
__forceinline int
LinuxInterlocked::CompareExchange(int volatile* dest, int exchange, int comparand)
{
     return __atomic_compare_exchange_n(dest, &comparand, exchange, 0, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
}

} // namespace Win360
//------------------------------------------------------------------------------
