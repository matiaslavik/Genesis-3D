set(Mono_ROOT "" CACHE PATH "(optional) root directory of where Mono is installed/built")

if(WIN32)
    if(Mono_ROOT AND EXISTS "${Mono_ROOT}" AND FALSE)
        message("Mono_ROOT is set. Searching for Mono in path: " + ${Mono_ROOT})
        find_program(
            Mono_EXECUTABLE mono
            PATHS ${Mono_ROOT} ${Mono_ROOT}/lib/mono/1.0
            PATH_SUFFIXES bin
            NO_DEFAULT_PATH
        )
        find_library(
            Mono_LIBRARY_RELEASE
            NAMES
                mono-2.0-sgen
            PATHS
                ${Mono_ROOT}
                "${Mono_ROOT}/lib"
                "${Mono_ROOT}/x64/lib"
                "${Mono_ROOT}/sgen/x64/lib"
                "${Mono_ROOT}/build/sgen/x64/lib"
                "${Mono_ROOT}/msvc/build/sgen/x64/lib"
            PATH_SUFFIXES
                "Release"
        )
        find_library(
            Mono_LIBRARY_DEBUG
            NAMES
                mono-2.0-sgen
            PATHS
                ${Mono_ROOT}
                "${Mono_ROOT}/lib"
                "${Mono_ROOT}/x64/lib"
                "${Mono_ROOT}/sgen/x64/lib"
                "${Mono_ROOT}/build/sgen/x64/lib"
                "${Mono_ROOT}/msvc/build/sgen/x64/lib"
            PATH_SUFFIXES
                "Debug"
        )
        find_path(
            Mono_INCLUDE_DIR
            mono/jit/jit.h
            PATHS
                "${Mono_ROOT}/msvc/include"
                "${Mono_ROOT}/include"
                "${Mono_ROOT}"
                ${Mono_ROOT}
        )
        find_file(
            Mono_BINARY_RELEASE
            mono-2.0-sgen.dll
            PATHS
                "${Mono_ROOT}/msvc/include"
                "${Mono_ROOT}/include"
                "${Mono_ROOT}"
                ${Mono_ROOT}
            PATH_SUFFIXES
                "Release"
        )
        find_file(
            Mono_BINARY_DEBUG
            mono-2.0-sgen.dll
            PATHS
                "${Mono_ROOT}/msvc/include"
                "${Mono_ROOT}/include"
                "${Mono_ROOT}"
                ${Mono_ROOT}
            PATH_SUFFIXES
                "Debug"
        )
    else()
        if(CMAKE_SIZEOF_VOID_P EQUAL 8)
            set(Mono_INSTALL_DIR_TEMP "$ENV{ProgramFiles}/Mono")
        else()
            set(Mono_INSTALL_DIR_TEMP "$ENV{ProgramFiles\(x86\)}/Mono")
        endif()
        if(EXISTS "${Mono_INSTALL_DIR_TEMP}")
            find_library(
                Mono_LIBRARY_RELEASE
                NAMES
                    mono-2.0-sgen
                PATHS
                    "${Mono_INSTALL_DIR_TEMP}/lib"
            )
            set(Mono_LIBRARY_DEBUG ${Mono_LIBRARY_RELEASE})
            find_path(
                Mono_INCLUDE_DIR
                mono/jit/jit.h
                PATHS
                    "${Mono_INSTALL_DIR_TEMP}/include/mono-2.0"
            )
            find_file(
                Mono_BINARY_RELEASE
                mono-2.0-sgen.dll
                PATHS
                    "${Mono_INSTALL_DIR_TEMP}/bin"
            )
            set(Mono_BINARY_DEBUG ${Mono_BINARY_RELEASE})
        endif()
    endif()
endif()
