IF ( NOT WINDOWS_BUILD )
	SET(EXCLUDE_SOUNDSYSTEM "ON") # LAVIK: TODO
ENDIF()

SET(EXCLUDE_PHYSICS "ON") # LAVIK: TODO
add_compile_definitions(MYGUI_DONT_REPLACE_NULLPTR)
add_compile_definitions(__PROJECTOR_COMMIT__) # LAVIK: TODO
#add_compile_definitions(__SCRIPT_COMMIT__)
add_compile_definitions(__GLSLOPT_COMMIT__) # LAVIK: : Build GLSL optimizer!

IF( EXCLUDE_PHYSICS )
	add_compile_definitions(__PHYSX_COMMIT__)
ENDIF()
IF( EXCLUDE_SOUNDSYSTEM )
	add_compile_definitions(__SOUND_COMMIT__) # LAVIK: : Update OpenAL (can only build on Windows atm..)
ENDIF()

IF ( WINDOWS_BUILD )
	add_compile_definitions(RENDERDEVICE_D3D9)
	#add_compile_definitions(RENDERDEVICE_OPENGLES)
	add_compile_definitions(USE_NATIVE_WINDOW) # Do this elsewhere?
ELSEIF ( ANDROID_BUILD OR APPLE_BUILD OR LINUX_BUILD )
	add_compile_definitions(RENDERDEVICE_OPENGLES)
ELSE()
	add_compile_definitions(RENDERDEVICE_NULL)
ENDIF()

IF ( LINUX_BUILD )
	add_compile_definitions(__LINUX__) # LAVIK: TODO
ENDIF()
